'use strict';
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssMin = require('gulp-minify-css'),
    rimraf = require('rimraf'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

    var path = {
        build:{
            html: 'build/',
            css: 'build/css/',
            js: 'build/js/',
        },
        src:{
            html: 'src/*.html',
            js: 'src/js/main.js',
            style: 'src/style/main.scss',
        },
        watch:{
            html: 'src/**/*.html',
            js: 'src/**/*.js',
            style: 'src/style/**/*.scss',
        },
        clean: './build'
    }

    gulp.task('webserver', function(){
        browserSync({
            server: {
                baseDir: './build'
            },
            host: 'localhost',
            port: 3000
        });
    });

    gulp.task('html:build', function(done){
        gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
        done();
    });

    gulp.task('js:build', function(done){
        gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(sourceMaps.init())
        .pipe(uglify())
        .pipe(sourceMaps.write())
        .on('error', function(err) {
            const message = err.message || '';
            const errName = err.name || '';
            const codeFrame = err.codeFrame || '';
            gutil.log(gutil.colors.red.bold('[JS babel error]')+' '+ gutil.colors.bgRed(errName));
            gutil.log(gutil.colors.bold('message:') +' '+ message);
            gutil.log(gutil.colors.bold('codeframe:') + '\n' + codeFrame);
            this.emit('end');
        })
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
        done();
    });

    gulp.task('style:build', function(done){
        gulp.src(path.src.style)
        .pipe(sourceMaps.init())
        .pipe(sass())
        .on('error', function(err) {
            const type = err.type || '';
            const message = err.message || '';
            const extract = err.extract || [];
            const line = err.line || '';
            const column = err.column || '';
            gutil.log(gutil.colors.red.bold('[Less error]') +' '+ gutil.colors.bgRed(type) +' ('+ line +':'+ column +')');
            gutil.log(gutil.colors.bold('message:') +' '+ message);
            gutil.log(gutil.colors.bold('codeframe:') +'\n'+ extract.join('\n'));
            this.emit('end');
        })
        .pipe(prefixer())
        .pipe(cssMin())
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
        done();
    });

    gulp.task('build', gulp.series([
        'html:build',
        'js:build',
        'style:build',
    ]));

    gulp.task('watch', function(done){
        gulp.watch(path.watch.js, gulp.series('js:build'));
        gulp.watch(path.watch.html, gulp.series('html:build'));
        gulp.watch(path.watch.style, gulp.series('style:build'));
        
        // done();
    });

    gulp.task('clean', function(callback){
        rimraf(path.clean, callback);
        callback();
    });

    gulp.task('default', gulp.parallel('build', 'webserver', 'watch'));